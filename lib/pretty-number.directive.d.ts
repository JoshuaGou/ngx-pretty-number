import { ElementRef } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
export declare class PrettyNumberDirective {
    private el;
    private num;
    acceptedKeys: string[];
    navigationKeys: string[];
    constructor(el: ElementRef, num: DecimalPipe);
    blockPaste(e: KeyboardEvent): void;
    drop(e: any): void;
    onKeyup($event: any): void;
    onKeydown(event: KeyboardEvent): void;
    /**
     * Replace specific character in a string at specified index
     * @param str String
     * @param index Number
     * @param replacement any
     */
    replaceAt(str: string, index: any, replacement: any): string;
    isValidMovementKeys($event: any): boolean;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<PrettyNumberDirective>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<PrettyNumberDirective, "[ngxPrettyNumber]", never, {}, {}, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5kaXJlY3RpdmUuZC50cyIsInNvdXJjZXMiOlsicHJldHR5LW51bWJlci5kaXJlY3RpdmUuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGVjaW1hbFBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuZXhwb3J0IGRlY2xhcmUgY2xhc3MgUHJldHR5TnVtYmVyRGlyZWN0aXZlIHtcbiAgICBwcml2YXRlIGVsO1xuICAgIHByaXZhdGUgbnVtO1xuICAgIGFjY2VwdGVkS2V5czogc3RyaW5nW107XG4gICAgbmF2aWdhdGlvbktleXM6IHN0cmluZ1tdO1xuICAgIGNvbnN0cnVjdG9yKGVsOiBFbGVtZW50UmVmLCBudW06IERlY2ltYWxQaXBlKTtcbiAgICBibG9ja1Bhc3RlKGU6IEtleWJvYXJkRXZlbnQpOiB2b2lkO1xuICAgIGRyb3AoZTogYW55KTogdm9pZDtcbiAgICBvbktleXVwKCRldmVudDogYW55KTogdm9pZDtcbiAgICBvbktleWRvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpOiB2b2lkO1xuICAgIC8qKlxuICAgICAqIFJlcGxhY2Ugc3BlY2lmaWMgY2hhcmFjdGVyIGluIGEgc3RyaW5nIGF0IHNwZWNpZmllZCBpbmRleFxuICAgICAqIEBwYXJhbSBzdHIgU3RyaW5nXG4gICAgICogQHBhcmFtIGluZGV4IE51bWJlclxuICAgICAqIEBwYXJhbSByZXBsYWNlbWVudCBhbnlcbiAgICAgKi9cbiAgICByZXBsYWNlQXQoc3RyOiBzdHJpbmcsIGluZGV4OiBhbnksIHJlcGxhY2VtZW50OiBhbnkpOiBzdHJpbmc7XG4gICAgaXNWYWxpZE1vdmVtZW50S2V5cygkZXZlbnQ6IGFueSk6IGJvb2xlYW47XG59XG4iXX0=
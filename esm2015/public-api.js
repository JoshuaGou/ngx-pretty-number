/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of pretty-number
 */
export { PrettyNumberDirective } from './lib/pretty-number.directive';
export { PrettyNumberModule } from './lib/pretty-number.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1wcmV0dHktbnVtYmVyLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUlBLHNDQUFjLCtCQUErQixDQUFDO0FBQzlDLG1DQUFjLDRCQUE0QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBwcmV0dHktbnVtYmVyXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvcHJldHR5LW51bWJlci5kaXJlY3RpdmUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvcHJldHR5LW51bWJlci5tb2R1bGUnO1xuIl19
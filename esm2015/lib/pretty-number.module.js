/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { PrettyNumberDirective } from './pretty-number.directive';
import { DecimalPipe } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
export class PrettyNumberModule {
}
PrettyNumberModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: PrettyNumberModule });
PrettyNumberModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function PrettyNumberModule_Factory(t) { return new (t || PrettyNumberModule)(); }, providers: [DecimalPipe], imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(PrettyNumberModule, { declarations: function () { return [PrettyNumberDirective]; }, exports: function () { return [PrettyNumberDirective]; } }); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberModule, [{
        type: NgModule,
        args: [{
                declarations: [PrettyNumberDirective],
                imports: [],
                exports: [PrettyNumberDirective],
                providers: [DecimalPipe]
            }]
    }], null, null); })();

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5tb2R1bGUuanMiLCJzb3VyY2VzIjpbIm5nOi9uZ3gtcHJldHR5LW51bWJlci9saWIvcHJldHR5LW51bWJlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xFLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQzs7QUFRNUMsTUFBTSxPQUFPLGtCQUFrQjs7O0NBQUksK0NBTmxDLFFBQVEsU0FBQyxrQkFDUixZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxrQkFDckMsT0FBTyxFQUFFLEVBQUUsa0JBQ1gsT0FBTyxFQUFFLENBQUM7U0FBcUIsQ0FBQyxrQkFDaEMsU0FBUyxFQUFFLENBQUMsV0FBVyxDQUFDLGNBQ3pCOzs7Ozs7Ozs7MEJBQ0kiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUHJldHR5TnVtYmVyRGlyZWN0aXZlIH0gZnJvbSAnLi9wcmV0dHktbnVtYmVyLmRpcmVjdGl2ZSc7XG5pbXBvcnQge0RlY2ltYWxQaXBlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtQcmV0dHlOdW1iZXJEaXJlY3RpdmVdLFxuICBpbXBvcnRzOiBbXSxcbiAgZXhwb3J0czogW1ByZXR0eU51bWJlckRpcmVjdGl2ZV0sXG4gIHByb3ZpZGVyczogW0RlY2ltYWxQaXBlXVxufSlcbmV4cG9ydCBjbGFzcyBQcmV0dHlOdW1iZXJNb2R1bGUgeyB9XG4iXX0=
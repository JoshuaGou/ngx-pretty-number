/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostListener } from '@angular/core';
import { DecimalPipe } from '@angular/common';
export class PrettyNumberDirective {
    /**
     * @param {?} el
     * @param {?} num
     */
    constructor(el, num) {
        this.el = el;
        this.num = num;
        this.acceptedKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', 'Backspace', 'ArrowLeft', 'ArrowRight'];
        this.navigationKeys = ['Backspace', 'ArrowLeft', 'ArrowRight'];
    }
    /**
     * @param {?} e
     * @return {?}
     */
    blockPaste(e) {
        e.preventDefault();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    drop(e) {
        e.preventDefault();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onKeyup($event) {
        /** @type {?} */
        let inputValue = this.el.nativeElement.value;
        /** @type {?} */
        const cursorPosition = this.el.nativeElement.selectionStart;
        inputValue = inputValue.replace(/,/g, '');
        if (inputValue.includes('.')) {
            /** @type {?} */
            const splitValue = inputValue.split('.');
            splitValue[0] = this.num.transform(+splitValue[0]); // Format with commas the value before decimal
            if (splitValue[1] === '') { // Value after dot is empty, consider only the value before dot
                this.el.nativeElement.value = splitValue[0].concat('.00');
            }
            else {
                this.el.nativeElement.value = splitValue.join('.');
            }
            // Maintaining cursor position
            if (this.isValidMovementKeys($event) && cursorPosition < this.el.nativeElement.value.indexOf('.')) {
                /** @type {?} */
                const cursorDepth = splitValue[0].length - cursorPosition;
                this.el.nativeElement.setSelectionRange(cursorPosition + cursorDepth, cursorPosition + cursorDepth);
            }
            else {
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
            }
        }
        else {
            if (inputValue !== '') {
                // Append '.00' if user enter something like $1 to show $1.00
                this.el.nativeElement.value = this.num.transform(+inputValue).concat('.00');
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition); // Maintaining cursor position
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeydown(event) {
        /** @type {?} */
        const inputValue = this.el.nativeElement.value;
        /** @type {?} */
        const cursorPosition = this.el.nativeElement.selectionStart;
        if (this.acceptedKeys.includes(event.key)) {
            // If user pressed dot and there is no dot already in the field
            if (event.key === '.' && inputValue.indexOf('.') === -1 && inputValue === '') {
                this.el.nativeElement.value = inputValue.concat('0');
            }
            else if (event.key === '.' && inputValue.includes('.')) {
                // Prevent two dot
                event.preventDefault();
            }
            if (inputValue.includes('.')) {
                /** @type {?} */
                const dotIndex = inputValue.indexOf('.');
                /** @type {?} */
                const split = inputValue.split('.');
                // NavigationKeys => Allowing left arrow, right arrow and backspace
                if (split[1].length >= 2 && (cursorPosition - 1) === (dotIndex + 2) && !this.navigationKeys.includes(event.key)) {
                    event.preventDefault();
                }
                if (split[0] === '0' && cursorPosition < inputValue.indexOf('.')) {
                    this.el.nativeElement.value = '.'.concat(split[1]);
                    this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
                }
            }
            // Skip dot (automatically move cursor to next number after decimal)
            if (inputValue.includes('.') && event.key === '.' && (cursorPosition) === inputValue.indexOf('.')) {
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
            }
            // Skip dot on backspace (keep decimal)
            if (inputValue.includes('.') && event.key === 'Backspace' && (cursorPosition) === inputValue.indexOf('.') + 1) {
                this.el.nativeElement.setSelectionRange(cursorPosition - 1, cursorPosition - 1);
            }
            // If user entered $12.<cursor>56, and cursor is at middle of dot and 5, then if user presses '9' then it will update the
            // value to $12.96
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 1) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
                event.preventDefault();
            }
            // If user entered $12.5<cursor>6, and cursor is at middle of 5 and 6, then if user presses '9' then it will update the
            // value to $12.59
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 2) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 2, cursorPosition + 2);
                event.preventDefault();
            }
        }
        else {
            event.preventDefault();
        }
    }
    /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    replaceAt(str, index, replacement) {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    isValidMovementKeys($event) {
        return $event.key !== '.' && $event.key !== 'ArrowLeft' && $event.key !== 'ArrowRight' && $event.key !== 'Backspace';
    }
}
PrettyNumberDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngxPrettyNumber]'
            },] }
];
/** @nocollapse */
PrettyNumberDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: DecimalPipe }
];
PrettyNumberDirective.propDecorators = {
    blockPaste: [{ type: HostListener, args: ['paste', ['$event'],] }],
    drop: [{ type: HostListener, args: ['drop', ['$event'],] }],
    onKeyup: [{ type: HostListener, args: ['keyup', ['$event'],] }],
    onKeydown: [{ type: HostListener, args: ['keydown', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    PrettyNumberDirective.prototype.acceptedKeys;
    /** @type {?} */
    PrettyNumberDirective.prototype.navigationKeys;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.num;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtcHJldHR5LW51bWJlci8iLCJzb3VyY2VzIjpbImxpYi9wcmV0dHktbnVtYmVyLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNsRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFLNUMsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFLaEMsWUFBb0IsRUFBYyxFQUFVLEdBQWdCO1FBQXhDLE9BQUUsR0FBRixFQUFFLENBQVk7UUFBVSxRQUFHLEdBQUgsR0FBRyxDQUFhO1FBSDVELGlCQUFZLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDL0csbUJBQWMsR0FBRyxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFHMUQsQ0FBQzs7Ozs7SUFFa0MsVUFBVSxDQUFDLENBQWdCO1FBQzVELENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVpQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFa0MsT0FBTyxDQUFDLE1BQU07O1lBQzNDLFVBQVUsR0FBVyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLOztjQUM5QyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYztRQUMzRCxVQUFVLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDMUMsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFOztrQkFDdEIsVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ3hDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsOENBQThDO1lBQ2xHLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLCtEQUErRDtnQkFDekYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEQ7WUFDRCw4QkFBOEI7WUFDOUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7O3NCQUMzRixXQUFXLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxjQUFjO2dCQUN6RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsV0FBVyxFQUFFLGNBQWMsR0FBRyxXQUFXLENBQUMsQ0FBQzthQUNyRztpQkFBTTtnQkFDTCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDLENBQUM7YUFDekU7U0FDRjthQUFNO1lBQ0wsSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO2dCQUNyQiw2REFBNkQ7Z0JBQzdELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDNUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsOEJBQThCO2FBQ3hHO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVvQyxTQUFTLENBQUMsS0FBb0I7O2NBQzNELFVBQVUsR0FBVyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLOztjQUNoRCxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYztRQUMzRCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN6QywrREFBK0Q7WUFDL0QsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLFVBQVUsS0FBSyxFQUFFLEVBQUU7Z0JBQzVFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3REO2lCQUFNLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDeEQsa0JBQWtCO2dCQUNsQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDeEI7WUFDRCxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7O3NCQUN0QixRQUFRLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O3NCQUNsQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLG1FQUFtRTtnQkFDbkUsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDL0csS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2lCQUN4QjtnQkFFRCxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2hFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNuRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDLENBQUM7aUJBQ3pFO2FBQ0Y7WUFDRCxvRUFBb0U7WUFDcEUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDakcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxHQUFHLENBQUMsRUFBRSxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDakY7WUFDRCx1Q0FBdUM7WUFDdkMsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssV0FBVyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUUsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2pGO1lBQ0QseUhBQXlIO1lBQ3pILGtCQUFrQjtZQUNsQixJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO21CQUM1QyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3hCO1lBQ0QsdUhBQXVIO1lBQ3ZILGtCQUFrQjtZQUNsQixJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO21CQUM1QyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3hCO1NBR0Y7YUFBTTtZQUNMLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7Ozs7Ozs7O0lBUUQsU0FBUyxDQUFDLEdBQVcsRUFBRSxLQUFLLEVBQUUsV0FBVztRQUN2QyxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxHQUFHLFdBQVcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDckYsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxNQUFNO1FBQ3hCLE9BQU8sTUFBTSxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxXQUFXLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxZQUFZLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxXQUFXLENBQUM7SUFDdkgsQ0FBQzs7O1lBcEhGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2FBQzlCOzs7O1lBTGtCLFVBQVU7WUFDckIsV0FBVzs7O3lCQWFoQixZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDO21CQUloQyxZQUFZLFNBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDO3NCQUkvQixZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDO3dCQTRCaEMsWUFBWSxTQUFDLFNBQVMsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7OztJQTFDbkMsNkNBQStHOztJQUMvRywrQ0FBMEQ7Ozs7O0lBRTlDLG1DQUFzQjs7Ozs7SUFBRSxvQ0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0RpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RGVjaW1hbFBpcGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tuZ3hQcmV0dHlOdW1iZXJdJ1xufSlcbmV4cG9ydCBjbGFzcyBQcmV0dHlOdW1iZXJEaXJlY3RpdmUge1xuXG4gIGFjY2VwdGVkS2V5cyA9IFsnMScsICcyJywgJzMnLCAnNCcsICc1JywgJzYnLCAnNycsICc4JywgJzknLCAnMCcsICcuJywgJ0JhY2tzcGFjZScsICdBcnJvd0xlZnQnLCAnQXJyb3dSaWdodCddO1xuICBuYXZpZ2F0aW9uS2V5cyA9IFsnQmFja3NwYWNlJywgJ0Fycm93TGVmdCcsICdBcnJvd1JpZ2h0J107XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbDogRWxlbWVudFJlZiwgcHJpdmF0ZSBudW06IERlY2ltYWxQaXBlKSB7XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCdwYXN0ZScsIFsnJGV2ZW50J10pIGJsb2NrUGFzdGUoZTogS2V5Ym9hcmRFdmVudCkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2Ryb3AnLCBbJyRldmVudCddKSBkcm9wKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCdrZXl1cCcsIFsnJGV2ZW50J10pIG9uS2V5dXAoJGV2ZW50KSB7XG4gICAgbGV0IGlucHV0VmFsdWU6IHN0cmluZyA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZTtcbiAgICBjb25zdCBjdXJzb3JQb3NpdGlvbiA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZWxlY3Rpb25TdGFydDtcbiAgICBpbnB1dFZhbHVlID0gaW5wdXRWYWx1ZS5yZXBsYWNlKC8sL2csICcnKTtcbiAgICBpZiAoaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpKSB7XG4gICAgICBjb25zdCBzcGxpdFZhbHVlID0gaW5wdXRWYWx1ZS5zcGxpdCgnLicpO1xuICAgICAgc3BsaXRWYWx1ZVswXSA9IHRoaXMubnVtLnRyYW5zZm9ybSgrc3BsaXRWYWx1ZVswXSk7IC8vIEZvcm1hdCB3aXRoIGNvbW1hcyB0aGUgdmFsdWUgYmVmb3JlIGRlY2ltYWxcbiAgICAgIGlmIChzcGxpdFZhbHVlWzFdID09PSAnJykgeyAvLyBWYWx1ZSBhZnRlciBkb3QgaXMgZW1wdHksIGNvbnNpZGVyIG9ubHkgdGhlIHZhbHVlIGJlZm9yZSBkb3RcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gc3BsaXRWYWx1ZVswXS5jb25jYXQoJy4wMCcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gc3BsaXRWYWx1ZS5qb2luKCcuJyk7XG4gICAgICB9XG4gICAgICAvLyBNYWludGFpbmluZyBjdXJzb3IgcG9zaXRpb25cbiAgICAgIGlmICh0aGlzLmlzVmFsaWRNb3ZlbWVudEtleXMoJGV2ZW50KSAmJiBjdXJzb3JQb3NpdGlvbiA8IHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZS5pbmRleE9mKCcuJykpIHtcbiAgICAgICAgY29uc3QgY3Vyc29yRGVwdGggPSBzcGxpdFZhbHVlWzBdLmxlbmd0aCAtIGN1cnNvclBvc2l0aW9uO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24gKyBjdXJzb3JEZXB0aCwgY3Vyc29yUG9zaXRpb24gKyBjdXJzb3JEZXB0aCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24sIGN1cnNvclBvc2l0aW9uKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGlucHV0VmFsdWUgIT09ICcnKSB7XG4gICAgICAgIC8vIEFwcGVuZCAnLjAwJyBpZiB1c2VyIGVudGVyIHNvbWV0aGluZyBsaWtlICQxIHRvIHNob3cgJDEuMDBcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gdGhpcy5udW0udHJhbnNmb3JtKCtpbnB1dFZhbHVlKS5jb25jYXQoJy4wMCcpO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24sIGN1cnNvclBvc2l0aW9uKTsgLy8gTWFpbnRhaW5pbmcgY3Vyc29yIHBvc2l0aW9uXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcigna2V5ZG93bicsIFsnJGV2ZW50J10pIG9uS2V5ZG93bihldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGNvbnN0IGlucHV0VmFsdWU6IHN0cmluZyA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZTtcbiAgICBjb25zdCBjdXJzb3JQb3NpdGlvbiA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZWxlY3Rpb25TdGFydDtcbiAgICBpZiAodGhpcy5hY2NlcHRlZEtleXMuaW5jbHVkZXMoZXZlbnQua2V5KSkge1xuICAgICAgLy8gSWYgdXNlciBwcmVzc2VkIGRvdCBhbmQgdGhlcmUgaXMgbm8gZG90IGFscmVhZHkgaW4gdGhlIGZpZWxkXG4gICAgICBpZiAoZXZlbnQua2V5ID09PSAnLicgJiYgaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykgPT09IC0xICYmIGlucHV0VmFsdWUgPT09ICcnKSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IGlucHV0VmFsdWUuY29uY2F0KCcwJyk7XG4gICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleSA9PT0gJy4nICYmIGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKSkge1xuICAgICAgICAvLyBQcmV2ZW50IHR3byBkb3RcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICAgIGlmIChpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJykpIHtcbiAgICAgICAgY29uc3QgZG90SW5kZXggPSBpbnB1dFZhbHVlLmluZGV4T2YoJy4nKTtcbiAgICAgICAgY29uc3Qgc3BsaXQgPSBpbnB1dFZhbHVlLnNwbGl0KCcuJyk7XG4gICAgICAgIC8vIE5hdmlnYXRpb25LZXlzID0+IEFsbG93aW5nIGxlZnQgYXJyb3csIHJpZ2h0IGFycm93IGFuZCBiYWNrc3BhY2VcbiAgICAgICAgaWYgKHNwbGl0WzFdLmxlbmd0aCA+PSAyICYmIChjdXJzb3JQb3NpdGlvbiAtIDEpID09PSAoZG90SW5kZXggKyAyKSAmJiAhdGhpcy5uYXZpZ2F0aW9uS2V5cy5pbmNsdWRlcyhldmVudC5rZXkpKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzcGxpdFswXSA9PT0gJzAnICYmIGN1cnNvclBvc2l0aW9uIDwgaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykpIHtcbiAgICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnLicuY29uY2F0KHNwbGl0WzFdKTtcbiAgICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24sIGN1cnNvclBvc2l0aW9uKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLy8gU2tpcCBkb3QgKGF1dG9tYXRpY2FsbHkgbW92ZSBjdXJzb3IgdG8gbmV4dCBudW1iZXIgYWZ0ZXIgZGVjaW1hbClcbiAgICAgIGlmIChpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJykgJiYgZXZlbnQua2V5ID09PSAnLicgJiYgKGN1cnNvclBvc2l0aW9uKSA9PT0gaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykpIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uICsgMSwgY3Vyc29yUG9zaXRpb24gKyAxKTtcbiAgICAgIH1cbiAgICAgIC8vIFNraXAgZG90IG9uIGJhY2tzcGFjZSAoa2VlcCBkZWNpbWFsKVxuICAgICAgaWYgKGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKSAmJiBldmVudC5rZXkgPT09ICdCYWNrc3BhY2UnICYmIChjdXJzb3JQb3NpdGlvbikgPT09IGlucHV0VmFsdWUuaW5kZXhPZignLicpICsgMSkge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24gLSAxLCBjdXJzb3JQb3NpdGlvbiAtIDEpO1xuICAgICAgfVxuICAgICAgLy8gSWYgdXNlciBlbnRlcmVkICQxMi48Y3Vyc29yPjU2LCBhbmQgY3Vyc29yIGlzIGF0IG1pZGRsZSBvZiBkb3QgYW5kIDUsIHRoZW4gaWYgdXNlciBwcmVzc2VzICc5JyB0aGVuIGl0IHdpbGwgdXBkYXRlIHRoZVxuICAgICAgLy8gdmFsdWUgdG8gJDEyLjk2XG4gICAgICBpZiAoZXZlbnQua2V5ICE9PSAnLicgJiYgaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpXG4gICAgICAgICYmIChjdXJzb3JQb3NpdGlvbikgPT09IChpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSArIDEpICYmICF0aGlzLm5hdmlnYXRpb25LZXlzLmluY2x1ZGVzKGV2ZW50LmtleSkpIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gdGhpcy5yZXBsYWNlQXQoaW5wdXRWYWx1ZSwgY3Vyc29yUG9zaXRpb24sIGV2ZW50LmtleSk7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgnaW5wdXQnKSk7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiArIDEsIGN1cnNvclBvc2l0aW9uICsgMSk7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgICAvLyBJZiB1c2VyIGVudGVyZWQgJDEyLjU8Y3Vyc29yPjYsIGFuZCBjdXJzb3IgaXMgYXQgbWlkZGxlIG9mIDUgYW5kIDYsIHRoZW4gaWYgdXNlciBwcmVzc2VzICc5JyB0aGVuIGl0IHdpbGwgdXBkYXRlIHRoZVxuICAgICAgLy8gdmFsdWUgdG8gJDEyLjU5XG4gICAgICBpZiAoZXZlbnQua2V5ICE9PSAnLicgJiYgaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpXG4gICAgICAgICYmIChjdXJzb3JQb3NpdGlvbikgPT09IChpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSArIDIpICYmICF0aGlzLm5hdmlnYXRpb25LZXlzLmluY2x1ZGVzKGV2ZW50LmtleSkpIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gdGhpcy5yZXBsYWNlQXQoaW5wdXRWYWx1ZSwgY3Vyc29yUG9zaXRpb24sIGV2ZW50LmtleSk7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgnaW5wdXQnKSk7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiArIDIsIGN1cnNvclBvc2l0aW9uICsgMik7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG5cblxuICAgIH0gZWxzZSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXBsYWNlIHNwZWNpZmljIGNoYXJhY3RlciBpbiBhIHN0cmluZyBhdCBzcGVjaWZpZWQgaW5kZXhcbiAgICogQHBhcmFtIHN0ciBTdHJpbmdcbiAgICogQHBhcmFtIGluZGV4IE51bWJlclxuICAgKiBAcGFyYW0gcmVwbGFjZW1lbnQgYW55XG4gICAqL1xuICByZXBsYWNlQXQoc3RyOiBzdHJpbmcsIGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHJldHVybiBzdHIuc3Vic3RyKDAsIGluZGV4KSArIHJlcGxhY2VtZW50ICsgc3RyLnN1YnN0cihpbmRleCArIHJlcGxhY2VtZW50Lmxlbmd0aCk7XG4gIH1cblxuICBpc1ZhbGlkTW92ZW1lbnRLZXlzKCRldmVudCkge1xuICAgIHJldHVybiAkZXZlbnQua2V5ICE9PSAnLicgJiYgJGV2ZW50LmtleSAhPT0gJ0Fycm93TGVmdCcgJiYgJGV2ZW50LmtleSAhPT0gJ0Fycm93UmlnaHQnICYmICRldmVudC5rZXkgIT09ICdCYWNrc3BhY2UnO1xuICB9XG59XG4iXX0=
/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostListener } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/common';
export class PrettyNumberDirective {
    /**
     * @param {?} el
     * @param {?} num
     */
    constructor(el, num) {
        this.el = el;
        this.num = num;
        this.acceptedKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', 'Backspace', 'ArrowLeft', 'ArrowRight'];
        this.navigationKeys = ['Backspace', 'ArrowLeft', 'ArrowRight'];
    }
    /**
     * @param {?} e
     * @return {?}
     */
    blockPaste(e) {
        e.preventDefault();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    drop(e) {
        e.preventDefault();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onKeyup($event) {
        /** @type {?} */
        let inputValue = this.el.nativeElement.value;
        /** @type {?} */
        const cursorPosition = this.el.nativeElement.selectionStart;
        inputValue = inputValue.replace(/,/g, '');
        if (inputValue.includes('.')) {
            /** @type {?} */
            const splitValue = inputValue.split('.');
            splitValue[0] = this.num.transform(+splitValue[0]); // Format with commas the value before decimal
            if (splitValue[1] === '') { // Value after dot is empty, consider only the value before dot
                this.el.nativeElement.value = splitValue[0].concat('.00');
            }
            else {
                this.el.nativeElement.value = splitValue.join('.');
            }
            // Maintaining cursor position
            if (this.isValidMovementKeys($event) && cursorPosition < this.el.nativeElement.value.indexOf('.')) {
                /** @type {?} */
                const cursorDepth = splitValue[0].length - cursorPosition;
                this.el.nativeElement.setSelectionRange(cursorPosition + cursorDepth, cursorPosition + cursorDepth);
            }
            else {
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
            }
        }
        else {
            if (inputValue !== '') {
                // Append '.00' if user enter something like $1 to show $1.00
                this.el.nativeElement.value = this.num.transform(+inputValue).concat('.00');
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition); // Maintaining cursor position
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeydown(event) {
        /** @type {?} */
        const inputValue = this.el.nativeElement.value;
        /** @type {?} */
        const cursorPosition = this.el.nativeElement.selectionStart;
        if (this.acceptedKeys.includes(event.key)) {
            // If user pressed dot and there is no dot already in the field
            if (event.key === '.' && inputValue.indexOf('.') === -1 && inputValue === '') {
                this.el.nativeElement.value = inputValue.concat('0');
            }
            else if (event.key === '.' && inputValue.includes('.')) {
                // Prevent two dot
                event.preventDefault();
            }
            if (inputValue.includes('.')) {
                /** @type {?} */
                const dotIndex = inputValue.indexOf('.');
                /** @type {?} */
                const split = inputValue.split('.');
                // NavigationKeys => Allowing left arrow, right arrow and backspace
                if (split[1].length >= 2 && (cursorPosition - 1) === (dotIndex + 2) && !this.navigationKeys.includes(event.key)) {
                    event.preventDefault();
                }
                if (split[0] === '0' && cursorPosition < inputValue.indexOf('.')) {
                    this.el.nativeElement.value = '.'.concat(split[1]);
                    this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
                }
            }
            // Skip dot (automatically move cursor to next number after decimal)
            if (inputValue.includes('.') && event.key === '.' && (cursorPosition) === inputValue.indexOf('.')) {
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
            }
            // Skip dot on backspace (keep decimal)
            if (inputValue.includes('.') && event.key === 'Backspace' && (cursorPosition) === inputValue.indexOf('.') + 1) {
                this.el.nativeElement.setSelectionRange(cursorPosition - 1, cursorPosition - 1);
            }
            // If user entered $12.<cursor>56, and cursor is at middle of dot and 5, then if user presses '9' then it will update the
            // value to $12.96
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 1) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
                event.preventDefault();
            }
            // If user entered $12.5<cursor>6, and cursor is at middle of 5 and 6, then if user presses '9' then it will update the
            // value to $12.59
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 2) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 2, cursorPosition + 2);
                event.preventDefault();
            }
        }
        else {
            event.preventDefault();
        }
    }
    /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    replaceAt(str, index, replacement) {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    isValidMovementKeys($event) {
        return $event.key !== '.' && $event.key !== 'ArrowLeft' && $event.key !== 'ArrowRight' && $event.key !== 'Backspace';
    }
}
PrettyNumberDirective.ɵfac = function PrettyNumberDirective_Factory(t) { return new (t || PrettyNumberDirective)(ɵngcc0.ɵɵdirectiveInject(ɵngcc0.ElementRef), ɵngcc0.ɵɵdirectiveInject(ɵngcc1.DecimalPipe)); };
PrettyNumberDirective.ɵdir = ɵngcc0.ɵɵdefineDirective({ type: PrettyNumberDirective, selectors: [["", "ngxPrettyNumber", ""]], hostBindings: function PrettyNumberDirective_HostBindings(rf, ctx) { if (rf & 1) {
        ɵngcc0.ɵɵlistener("paste", function PrettyNumberDirective_paste_HostBindingHandler($event) { return ctx.blockPaste($event); })("drop", function PrettyNumberDirective_drop_HostBindingHandler($event) { return ctx.drop($event); })("keyup", function PrettyNumberDirective_keyup_HostBindingHandler($event) { return ctx.onKeyup($event); })("keydown", function PrettyNumberDirective_keydown_HostBindingHandler($event) { return ctx.onKeydown($event); });
    } } });
/** @nocollapse */
PrettyNumberDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: DecimalPipe }
];
PrettyNumberDirective.propDecorators = {
    blockPaste: [{ type: HostListener, args: ['paste', ['$event'],] }],
    drop: [{ type: HostListener, args: ['drop', ['$event'],] }],
    onKeyup: [{ type: HostListener, args: ['keyup', ['$event'],] }],
    onKeydown: [{ type: HostListener, args: ['keydown', ['$event'],] }]
};
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberDirective, [{
        type: Directive,
        args: [{
                selector: '[ngxPrettyNumber]'
            }]
    }], function () { return [{ type: ɵngcc0.ElementRef }, { type: ɵngcc1.DecimalPipe }]; }, { blockPaste: [{
            type: HostListener,
            args: ['paste', ['$event']]
        }], drop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }], onKeyup: [{
            type: HostListener,
            args: ['keyup', ['$event']]
        }], onKeydown: [{
            type: HostListener,
            args: ['keydown', ['$event']]
        }] }); })();
if (false) {
    /** @type {?} */
    PrettyNumberDirective.prototype.acceptedKeys;
    /** @type {?} */
    PrettyNumberDirective.prototype.navigationKeys;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.num;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VzIjpbIm5nOi9uZ3gtcHJldHR5LW51bWJlci9saWIvcHJldHR5LW51bWJlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDbEUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGlCQUFpQixDQUFDOzs7QUFLNUMsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFLaEMsWUFBb0IsRUFBYyxFQUFVLEdBQWdCO1FBQXhDLE9BQUUsR0FBRixFQUFFLENBQVk7UUFBVSxRQUFHLEdBQUgsR0FBRyxDQUFhO1FBSDVELGlCQUFZLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDL0csbUJBQWMsR0FBRyxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFHMUQsQ0FBQzs7Ozs7SUFFa0MsVUFBVSxDQUFDLENBQWdCO1FBQzVELENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVpQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFa0MsT0FBTyxDQUFDLE1BQU07O1lBQzNDLFVBQVUsR0FBVyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLOztjQUM5QyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYztRQUMzRCxVQUFVLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDMUMsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFOztrQkFDdEIsVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ3hDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsOENBQThDO1lBQ2xHLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLCtEQUErRDtnQkFDekYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEQ7WUFDRCw4QkFBOEI7WUFDOUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7O3NCQUMzRixXQUFXLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxjQUFjO2dCQUN6RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsV0FBVyxFQUFFLGNBQWMsR0FBRyxXQUFXLENBQUMsQ0FBQzthQUNyRztpQkFBTTtnQkFDTCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDLENBQUM7YUFDekU7U0FDRjthQUFNO1lBQ0wsSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO2dCQUNyQiw2REFBNkQ7Z0JBQzdELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDNUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsOEJBQThCO2FBQ3hHO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVvQyxTQUFTLENBQUMsS0FBb0I7O2NBQzNELFVBQVUsR0FBVyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLOztjQUNoRCxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYztRQUMzRCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN6QywrREFBK0Q7WUFDL0QsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLFVBQVUsS0FBSyxFQUFFLEVBQUU7Z0JBQzVFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3REO2lCQUFNLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDeEQsa0JBQWtCO2dCQUNsQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDeEI7WUFDRCxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7O3NCQUN0QixRQUFRLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O3NCQUNsQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLG1FQUFtRTtnQkFDbkUsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDL0csS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2lCQUN4QjtnQkFFRCxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2hFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNuRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDLENBQUM7aUJBQ3pFO2FBQ0Y7WUFDRCxvRUFBb0U7WUFDcEUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDakcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxHQUFHLENBQUMsRUFBRSxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDakY7WUFDRCx1Q0FBdUM7WUFDdkMsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssV0FBVyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUUsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2pGO1lBQ0QseUhBQXlIO1lBQ3pILGtCQUFrQjtZQUNsQixJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO21CQUM1QyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3hCO1lBQ0QsdUhBQXVIO1lBQ3ZILGtCQUFrQjtZQUNsQixJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO21CQUM1QyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEYsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3hCO1NBR0Y7YUFBTTtZQUNMLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7Ozs7Ozs7O0lBUUQsU0FBUyxDQUFDLEdBQVcsRUFBRSxLQUFLLEVBQUUsV0FBVztRQUN2QyxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxHQUFHLFdBQVcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDckYsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxNQUFNO1FBQ3hCLE9BQU8sTUFBTSxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxXQUFXLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxZQUFZLElBQUksTUFBTSxDQUFDLEdBQUcsS0FBSyxXQUFXLENBQUM7SUFDdkgsQ0FBQzs7Ozs7Q0FDRixVQWxISTs7b0JBSEosU0FBUyxTQUFDO1NBQ1QsUUFBUSxFQUFFLFBBSk8sVUFBVTtZQUlFLEFBSHZCLFdBQVc7QUFJbEI7O3lCQVNFLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7bUJBSWhDLFlBQVksU0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUM7c0JBSS9CLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7d0JBNEJoQyxZQUFZLFNBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O29CQUFROzs7SUExQzNDLDZDQUErRzs7SUFDL0csK0NBQTBEOzs7OztJQUU5QyxtQ0FBc0I7Ozs7O0lBQUUsb0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0RlY2ltYWxQaXBlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbbmd4UHJldHR5TnVtYmVyXSdcbn0pXG5leHBvcnQgY2xhc3MgUHJldHR5TnVtYmVyRGlyZWN0aXZlIHtcblxuICBhY2NlcHRlZEtleXMgPSBbJzEnLCAnMicsICczJywgJzQnLCAnNScsICc2JywgJzcnLCAnOCcsICc5JywgJzAnLCAnLicsICdCYWNrc3BhY2UnLCAnQXJyb3dMZWZ0JywgJ0Fycm93UmlnaHQnXTtcbiAgbmF2aWdhdGlvbktleXMgPSBbJ0JhY2tzcGFjZScsICdBcnJvd0xlZnQnLCAnQXJyb3dSaWdodCddO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsIHByaXZhdGUgbnVtOiBEZWNpbWFsUGlwZSkge1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcigncGFzdGUnLCBbJyRldmVudCddKSBibG9ja1Bhc3RlKGU6IEtleWJvYXJkRXZlbnQpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCdkcm9wJywgWyckZXZlbnQnXSkgZHJvcChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcigna2V5dXAnLCBbJyRldmVudCddKSBvbktleXVwKCRldmVudCkge1xuICAgIGxldCBpbnB1dFZhbHVlOiBzdHJpbmcgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWU7XG4gICAgY29uc3QgY3Vyc29yUG9zaXRpb24gPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2VsZWN0aW9uU3RhcnQ7XG4gICAgaW5wdXRWYWx1ZSA9IGlucHV0VmFsdWUucmVwbGFjZSgvLC9nLCAnJyk7XG4gICAgaWYgKGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKSkge1xuICAgICAgY29uc3Qgc3BsaXRWYWx1ZSA9IGlucHV0VmFsdWUuc3BsaXQoJy4nKTtcbiAgICAgIHNwbGl0VmFsdWVbMF0gPSB0aGlzLm51bS50cmFuc2Zvcm0oK3NwbGl0VmFsdWVbMF0pOyAvLyBGb3JtYXQgd2l0aCBjb21tYXMgdGhlIHZhbHVlIGJlZm9yZSBkZWNpbWFsXG4gICAgICBpZiAoc3BsaXRWYWx1ZVsxXSA9PT0gJycpIHsgLy8gVmFsdWUgYWZ0ZXIgZG90IGlzIGVtcHR5LCBjb25zaWRlciBvbmx5IHRoZSB2YWx1ZSBiZWZvcmUgZG90XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHNwbGl0VmFsdWVbMF0uY29uY2F0KCcuMDAnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHNwbGl0VmFsdWUuam9pbignLicpO1xuICAgICAgfVxuICAgICAgLy8gTWFpbnRhaW5pbmcgY3Vyc29yIHBvc2l0aW9uXG4gICAgICBpZiAodGhpcy5pc1ZhbGlkTW92ZW1lbnRLZXlzKCRldmVudCkgJiYgY3Vyc29yUG9zaXRpb24gPCB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUuaW5kZXhPZignLicpKSB7XG4gICAgICAgIGNvbnN0IGN1cnNvckRlcHRoID0gc3BsaXRWYWx1ZVswXS5sZW5ndGggLSBjdXJzb3JQb3NpdGlvbjtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uICsgY3Vyc29yRGVwdGgsIGN1cnNvclBvc2l0aW9uICsgY3Vyc29yRGVwdGgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uLCBjdXJzb3JQb3NpdGlvbik7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChpbnB1dFZhbHVlICE9PSAnJykge1xuICAgICAgICAvLyBBcHBlbmQgJy4wMCcgaWYgdXNlciBlbnRlciBzb21ldGhpbmcgbGlrZSAkMSB0byBzaG93ICQxLjAwXG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHRoaXMubnVtLnRyYW5zZm9ybSgraW5wdXRWYWx1ZSkuY29uY2F0KCcuMDAnKTtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uLCBjdXJzb3JQb3NpdGlvbik7IC8vIE1haW50YWluaW5nIGN1cnNvciBwb3NpdGlvblxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2tleWRvd24nLCBbJyRldmVudCddKSBvbktleWRvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcbiAgICBjb25zdCBpbnB1dFZhbHVlOiBzdHJpbmcgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWU7XG4gICAgY29uc3QgY3Vyc29yUG9zaXRpb24gPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2VsZWN0aW9uU3RhcnQ7XG4gICAgaWYgKHRoaXMuYWNjZXB0ZWRLZXlzLmluY2x1ZGVzKGV2ZW50LmtleSkpIHtcbiAgICAgIC8vIElmIHVzZXIgcHJlc3NlZCBkb3QgYW5kIHRoZXJlIGlzIG5vIGRvdCBhbHJlYWR5IGluIHRoZSBmaWVsZFxuICAgICAgaWYgKGV2ZW50LmtleSA9PT0gJy4nICYmIGlucHV0VmFsdWUuaW5kZXhPZignLicpID09PSAtMSAmJiBpbnB1dFZhbHVlID09PSAnJykge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBpbnB1dFZhbHVlLmNvbmNhdCgnMCcpO1xuICAgICAgfSBlbHNlIGlmIChldmVudC5rZXkgPT09ICcuJyAmJiBpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJykpIHtcbiAgICAgICAgLy8gUHJldmVudCB0d28gZG90XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgICBpZiAoaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpKSB7XG4gICAgICAgIGNvbnN0IGRvdEluZGV4ID0gaW5wdXRWYWx1ZS5pbmRleE9mKCcuJyk7XG4gICAgICAgIGNvbnN0IHNwbGl0ID0gaW5wdXRWYWx1ZS5zcGxpdCgnLicpO1xuICAgICAgICAvLyBOYXZpZ2F0aW9uS2V5cyA9PiBBbGxvd2luZyBsZWZ0IGFycm93LCByaWdodCBhcnJvdyBhbmQgYmFja3NwYWNlXG4gICAgICAgIGlmIChzcGxpdFsxXS5sZW5ndGggPj0gMiAmJiAoY3Vyc29yUG9zaXRpb24gLSAxKSA9PT0gKGRvdEluZGV4ICsgMikgJiYgIXRoaXMubmF2aWdhdGlvbktleXMuaW5jbHVkZXMoZXZlbnQua2V5KSkge1xuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc3BsaXRbMF0gPT09ICcwJyAmJiBjdXJzb3JQb3NpdGlvbiA8IGlucHV0VmFsdWUuaW5kZXhPZignLicpKSB7XG4gICAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gJy4nLmNvbmNhdChzcGxpdFsxXSk7XG4gICAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uLCBjdXJzb3JQb3NpdGlvbik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8vIFNraXAgZG90IChhdXRvbWF0aWNhbGx5IG1vdmUgY3Vyc29yIHRvIG5leHQgbnVtYmVyIGFmdGVyIGRlY2ltYWwpXG4gICAgICBpZiAoaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpICYmIGV2ZW50LmtleSA9PT0gJy4nICYmIChjdXJzb3JQb3NpdGlvbikgPT09IGlucHV0VmFsdWUuaW5kZXhPZignLicpKSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiArIDEsIGN1cnNvclBvc2l0aW9uICsgMSk7XG4gICAgICB9XG4gICAgICAvLyBTa2lwIGRvdCBvbiBiYWNrc3BhY2UgKGtlZXAgZGVjaW1hbClcbiAgICAgIGlmIChpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJykgJiYgZXZlbnQua2V5ID09PSAnQmFja3NwYWNlJyAmJiAoY3Vyc29yUG9zaXRpb24pID09PSBpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSArIDEpIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uIC0gMSwgY3Vyc29yUG9zaXRpb24gLSAxKTtcbiAgICAgIH1cbiAgICAgIC8vIElmIHVzZXIgZW50ZXJlZCAkMTIuPGN1cnNvcj41NiwgYW5kIGN1cnNvciBpcyBhdCBtaWRkbGUgb2YgZG90IGFuZCA1LCB0aGVuIGlmIHVzZXIgcHJlc3NlcyAnOScgdGhlbiBpdCB3aWxsIHVwZGF0ZSB0aGVcbiAgICAgIC8vIHZhbHVlIHRvICQxMi45NlxuICAgICAgaWYgKGV2ZW50LmtleSAhPT0gJy4nICYmIGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKVxuICAgICAgICAmJiAoY3Vyc29yUG9zaXRpb24pID09PSAoaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykgKyAxKSAmJiAhdGhpcy5uYXZpZ2F0aW9uS2V5cy5pbmNsdWRlcyhldmVudC5rZXkpKSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHRoaXMucmVwbGFjZUF0KGlucHV0VmFsdWUsIGN1cnNvclBvc2l0aW9uLCBldmVudC5rZXkpO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChuZXcgRXZlbnQoJ2lucHV0JykpO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24gKyAxLCBjdXJzb3JQb3NpdGlvbiArIDEpO1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgICAgLy8gSWYgdXNlciBlbnRlcmVkICQxMi41PGN1cnNvcj42LCBhbmQgY3Vyc29yIGlzIGF0IG1pZGRsZSBvZiA1IGFuZCA2LCB0aGVuIGlmIHVzZXIgcHJlc3NlcyAnOScgdGhlbiBpdCB3aWxsIHVwZGF0ZSB0aGVcbiAgICAgIC8vIHZhbHVlIHRvICQxMi41OVxuICAgICAgaWYgKGV2ZW50LmtleSAhPT0gJy4nICYmIGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKVxuICAgICAgICAmJiAoY3Vyc29yUG9zaXRpb24pID09PSAoaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykgKyAyKSAmJiAhdGhpcy5uYXZpZ2F0aW9uS2V5cy5pbmNsdWRlcyhldmVudC5rZXkpKSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHRoaXMucmVwbGFjZUF0KGlucHV0VmFsdWUsIGN1cnNvclBvc2l0aW9uLCBldmVudC5rZXkpO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChuZXcgRXZlbnQoJ2lucHV0JykpO1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24gKyAyLCBjdXJzb3JQb3NpdGlvbiArIDIpO1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuXG5cbiAgICB9IGVsc2Uge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVwbGFjZSBzcGVjaWZpYyBjaGFyYWN0ZXIgaW4gYSBzdHJpbmcgYXQgc3BlY2lmaWVkIGluZGV4XG4gICAqIEBwYXJhbSBzdHIgU3RyaW5nXG4gICAqIEBwYXJhbSBpbmRleCBOdW1iZXJcbiAgICogQHBhcmFtIHJlcGxhY2VtZW50IGFueVxuICAgKi9cbiAgcmVwbGFjZUF0KHN0cjogc3RyaW5nLCBpbmRleCwgcmVwbGFjZW1lbnQpIHtcbiAgICByZXR1cm4gc3RyLnN1YnN0cigwLCBpbmRleCkgKyByZXBsYWNlbWVudCArIHN0ci5zdWJzdHIoaW5kZXggKyByZXBsYWNlbWVudC5sZW5ndGgpO1xuICB9XG5cbiAgaXNWYWxpZE1vdmVtZW50S2V5cygkZXZlbnQpIHtcbiAgICByZXR1cm4gJGV2ZW50LmtleSAhPT0gJy4nICYmICRldmVudC5rZXkgIT09ICdBcnJvd0xlZnQnICYmICRldmVudC5rZXkgIT09ICdBcnJvd1JpZ2h0JyAmJiAkZXZlbnQua2V5ICE9PSAnQmFja3NwYWNlJztcbiAgfVxufVxuIl19
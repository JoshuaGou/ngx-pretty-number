import { Directive, ElementRef, HostListener, NgModule } from '@angular/core';
import { DecimalPipe } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/common';
var PrettyNumberDirective = /** @class */ (function () {
    function PrettyNumberDirective(el, num) {
        this.el = el;
        this.num = num;
        this.acceptedKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', 'Backspace', 'ArrowLeft', 'ArrowRight'];
        this.navigationKeys = ['Backspace', 'ArrowLeft', 'ArrowRight'];
    }
    /**
     * @param {?} e
     * @return {?}
     */
    PrettyNumberDirective.prototype.blockPaste = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.preventDefault();
    };
    /**
     * @param {?} e
     * @return {?}
     */
    PrettyNumberDirective.prototype.drop = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.preventDefault();
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PrettyNumberDirective.prototype.onKeyup = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        /** @type {?} */
        var inputValue = this.el.nativeElement.value;
        /** @type {?} */
        var cursorPosition = this.el.nativeElement.selectionStart;
        inputValue = inputValue.replace(/,/g, '');
        if (inputValue.includes('.')) {
            /** @type {?} */
            var splitValue = inputValue.split('.');
            splitValue[0] = this.num.transform(+splitValue[0]); // Format with commas the value before decimal
            if (splitValue[1] === '') { // Value after dot is empty, consider only the value before dot
                this.el.nativeElement.value = splitValue[0].concat('.00');
            }
            else {
                this.el.nativeElement.value = splitValue.join('.');
            }
            // Maintaining cursor position
            if (this.isValidMovementKeys($event) && cursorPosition < this.el.nativeElement.value.indexOf('.')) {
                /** @type {?} */
                var cursorDepth = splitValue[0].length - cursorPosition;
                this.el.nativeElement.setSelectionRange(cursorPosition + cursorDepth, cursorPosition + cursorDepth);
            }
            else {
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
            }
        }
        else {
            if (inputValue !== '') {
                // Append '.00' if user enter something like $1 to show $1.00
                this.el.nativeElement.value = this.num.transform(+inputValue).concat('.00');
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition); // Maintaining cursor position
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    PrettyNumberDirective.prototype.onKeydown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var inputValue = this.el.nativeElement.value;
        /** @type {?} */
        var cursorPosition = this.el.nativeElement.selectionStart;
        if (this.acceptedKeys.includes(event.key)) {
            // If user pressed dot and there is no dot already in the field
            if (event.key === '.' && inputValue.indexOf('.') === -1 && inputValue === '') {
                this.el.nativeElement.value = inputValue.concat('0');
            }
            else if (event.key === '.' && inputValue.includes('.')) {
                // Prevent two dot
                event.preventDefault();
            }
            if (inputValue.includes('.')) {
                /** @type {?} */
                var dotIndex = inputValue.indexOf('.');
                /** @type {?} */
                var split = inputValue.split('.');
                // NavigationKeys => Allowing left arrow, right arrow and backspace
                if (split[1].length >= 2 && (cursorPosition - 1) === (dotIndex + 2) && !this.navigationKeys.includes(event.key)) {
                    event.preventDefault();
                }
                if (split[0] === '0' && cursorPosition < inputValue.indexOf('.')) {
                    this.el.nativeElement.value = '.'.concat(split[1]);
                    this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
                }
            }
            // Skip dot (automatically move cursor to next number after decimal)
            if (inputValue.includes('.') && event.key === '.' && (cursorPosition) === inputValue.indexOf('.')) {
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
            }
            // Skip dot on backspace (keep decimal)
            if (inputValue.includes('.') && event.key === 'Backspace' && (cursorPosition) === inputValue.indexOf('.') + 1) {
                this.el.nativeElement.setSelectionRange(cursorPosition - 1, cursorPosition - 1);
            }
            // If user entered $12.<cursor>56, and cursor is at middle of dot and 5, then if user presses '9' then it will update the
            // value to $12.96
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 1) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
                event.preventDefault();
            }
            // If user entered $12.5<cursor>6, and cursor is at middle of 5 and 6, then if user presses '9' then it will update the
            // value to $12.59
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 2) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 2, cursorPosition + 2);
                event.preventDefault();
            }
        }
        else {
            event.preventDefault();
        }
    };
    /**
     * Replace specific character in a string at specified index
     * @param str String
     * @param index Number
     * @param replacement any
     */
    /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    PrettyNumberDirective.prototype.replaceAt = /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    function (str, index, replacement) {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PrettyNumberDirective.prototype.isValidMovementKeys = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        return $event.key !== '.' && $event.key !== 'ArrowLeft' && $event.key !== 'ArrowRight' && $event.key !== 'Backspace';
    };
    /** @nocollapse */
    PrettyNumberDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: DecimalPipe }
    ]; };
    PrettyNumberDirective.propDecorators = {
        blockPaste: [{ type: HostListener, args: ['paste', ['$event'],] }],
        drop: [{ type: HostListener, args: ['drop', ['$event'],] }],
        onKeyup: [{ type: HostListener, args: ['keyup', ['$event'],] }],
        onKeydown: [{ type: HostListener, args: ['keydown', ['$event'],] }]
    };
PrettyNumberDirective.ɵfac = function PrettyNumberDirective_Factory(t) { return new (t || PrettyNumberDirective)(ɵngcc0.ɵɵdirectiveInject(ɵngcc0.ElementRef), ɵngcc0.ɵɵdirectiveInject(ɵngcc1.DecimalPipe)); };
PrettyNumberDirective.ɵdir = ɵngcc0.ɵɵdefineDirective({ type: PrettyNumberDirective, selectors: [["", "ngxPrettyNumber", ""]], hostBindings: function PrettyNumberDirective_HostBindings(rf, ctx) { if (rf & 1) {
        ɵngcc0.ɵɵlistener("paste", function PrettyNumberDirective_paste_HostBindingHandler($event) { return ctx.blockPaste($event); })("drop", function PrettyNumberDirective_drop_HostBindingHandler($event) { return ctx.drop($event); })("keyup", function PrettyNumberDirective_keyup_HostBindingHandler($event) { return ctx.onKeyup($event); })("keydown", function PrettyNumberDirective_keydown_HostBindingHandler($event) { return ctx.onKeydown($event); });
    } } });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberDirective, [{
        type: Directive,
        args: [{
                selector: '[ngxPrettyNumber]'
            }]
    }], function () { return [{ type: ɵngcc0.ElementRef }, { type: ɵngcc1.DecimalPipe }]; }, { blockPaste: [{
            type: HostListener,
            args: ['paste', ['$event']]
        }], drop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }], onKeyup: [{
            type: HostListener,
            args: ['keyup', ['$event']]
        }], onKeydown: [{
            type: HostListener,
            args: ['keydown', ['$event']]
        }] }); })();
    return PrettyNumberDirective;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var PrettyNumberModule = /** @class */ (function () {
    function PrettyNumberModule() {
    }
PrettyNumberModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: PrettyNumberModule });
PrettyNumberModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function PrettyNumberModule_Factory(t) { return new (t || PrettyNumberModule)(); }, providers: [DecimalPipe], imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(PrettyNumberModule, { declarations: [PrettyNumberDirective], exports: [PrettyNumberDirective] }); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberModule, [{
        type: NgModule,
        args: [{
                declarations: [PrettyNumberDirective],
                imports: [],
                exports: [PrettyNumberDirective],
                providers: [DecimalPipe]
            }]
    }], function () { return []; }, null); })();
    return PrettyNumberModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: ngx-pretty-number.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PrettyNumberDirective, PrettyNumberModule };

//# sourceMappingURL=ngx-pretty-number.js.map
# ngxPrettyNumber

This library includes functionality to enable number only input field along with two decimal places. Package includes a single directive `ngxPrettyNumber` which
needs to be applied on input (type='text') field. Adding this will enable comma formatting as user start typing. Also, it will maintain two decimal places mask.

## Library demo

Demo can be found [here](https://ngxprettynumberdemo-87dd3.web.app/)

## How to use

Following are the three simple steps which you need to follow in order to use this library:
1. Install the library using the command `npm install ngx-pretty-number` in terminal.
2. Import the single library module in your application. `import { NgxPrettyNumberModule } from 'ngx-pretty-module'`. Also, add NgxPrettyNumberModule in your list of
imports.
3. Use in your html. `<input type='text' ngxPrettyNumberModule>`

Wohoo! that's it. Enjoy coding.

## Support/Help

If at any point you want help or want to give any sugesstion, feel free to contact me on my [Twitter handle](https://twitter.com/me_parwinder).


## Suggestion/Bugs notes

This library is created to solve the challenge Angular developer's face when working on formatting the number fields. Although utmost care has been taken to make it bug free, but there can be some unknown issues which you might face.
Your bugs/suggestions are heartly welcomed. 

*Please drop Git issues only which are related to the library.

## Contact me

If in case you get stuck in some issue, feel free to contact me on my [Twitter handle](https://twitter.com/me_parwinder).

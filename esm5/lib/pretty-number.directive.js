/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostListener } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/common';
var PrettyNumberDirective = /** @class */ (function () {
    function PrettyNumberDirective(el, num) {
        this.el = el;
        this.num = num;
        this.acceptedKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', 'Backspace', 'ArrowLeft', 'ArrowRight'];
        this.navigationKeys = ['Backspace', 'ArrowLeft', 'ArrowRight'];
    }
    /**
     * @param {?} e
     * @return {?}
     */
    PrettyNumberDirective.prototype.blockPaste = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.preventDefault();
    };
    /**
     * @param {?} e
     * @return {?}
     */
    PrettyNumberDirective.prototype.drop = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.preventDefault();
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PrettyNumberDirective.prototype.onKeyup = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        /** @type {?} */
        var inputValue = this.el.nativeElement.value;
        /** @type {?} */
        var cursorPosition = this.el.nativeElement.selectionStart;
        inputValue = inputValue.replace(/,/g, '');
        if (inputValue.includes('.')) {
            /** @type {?} */
            var splitValue = inputValue.split('.');
            splitValue[0] = this.num.transform(+splitValue[0]); // Format with commas the value before decimal
            if (splitValue[1] === '') { // Value after dot is empty, consider only the value before dot
                this.el.nativeElement.value = splitValue[0].concat('.00');
            }
            else {
                this.el.nativeElement.value = splitValue.join('.');
            }
            // Maintaining cursor position
            if (this.isValidMovementKeys($event) && cursorPosition < this.el.nativeElement.value.indexOf('.')) {
                /** @type {?} */
                var cursorDepth = splitValue[0].length - cursorPosition;
                this.el.nativeElement.setSelectionRange(cursorPosition + cursorDepth, cursorPosition + cursorDepth);
            }
            else {
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
            }
        }
        else {
            if (inputValue !== '') {
                // Append '.00' if user enter something like $1 to show $1.00
                this.el.nativeElement.value = this.num.transform(+inputValue).concat('.00');
                this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition); // Maintaining cursor position
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    PrettyNumberDirective.prototype.onKeydown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var inputValue = this.el.nativeElement.value;
        /** @type {?} */
        var cursorPosition = this.el.nativeElement.selectionStart;
        if (this.acceptedKeys.includes(event.key)) {
            // If user pressed dot and there is no dot already in the field
            if (event.key === '.' && inputValue.indexOf('.') === -1 && inputValue === '') {
                this.el.nativeElement.value = inputValue.concat('0');
            }
            else if (event.key === '.' && inputValue.includes('.')) {
                // Prevent two dot
                event.preventDefault();
            }
            if (inputValue.includes('.')) {
                /** @type {?} */
                var dotIndex = inputValue.indexOf('.');
                /** @type {?} */
                var split = inputValue.split('.');
                // NavigationKeys => Allowing left arrow, right arrow and backspace
                if (split[1].length >= 2 && (cursorPosition - 1) === (dotIndex + 2) && !this.navigationKeys.includes(event.key)) {
                    event.preventDefault();
                }
                if (split[0] === '0' && cursorPosition < inputValue.indexOf('.')) {
                    this.el.nativeElement.value = '.'.concat(split[1]);
                    this.el.nativeElement.setSelectionRange(cursorPosition, cursorPosition);
                }
            }
            // Skip dot (automatically move cursor to next number after decimal)
            if (inputValue.includes('.') && event.key === '.' && (cursorPosition) === inputValue.indexOf('.')) {
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
            }
            // Skip dot on backspace (keep decimal)
            if (inputValue.includes('.') && event.key === 'Backspace' && (cursorPosition) === inputValue.indexOf('.') + 1) {
                this.el.nativeElement.setSelectionRange(cursorPosition - 1, cursorPosition - 1);
            }
            // If user entered $12.<cursor>56, and cursor is at middle of dot and 5, then if user presses '9' then it will update the
            // value to $12.96
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 1) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
                event.preventDefault();
            }
            // If user entered $12.5<cursor>6, and cursor is at middle of 5 and 6, then if user presses '9' then it will update the
            // value to $12.59
            if (event.key !== '.' && inputValue.includes('.')
                && (cursorPosition) === (inputValue.indexOf('.') + 2) && !this.navigationKeys.includes(event.key)) {
                this.el.nativeElement.value = this.replaceAt(inputValue, cursorPosition, event.key);
                this.el.nativeElement.dispatchEvent(new Event('input'));
                this.el.nativeElement.setSelectionRange(cursorPosition + 2, cursorPosition + 2);
                event.preventDefault();
            }
        }
        else {
            event.preventDefault();
        }
    };
    /**
     * Replace specific character in a string at specified index
     * @param str String
     * @param index Number
     * @param replacement any
     */
    /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    PrettyNumberDirective.prototype.replaceAt = /**
     * Replace specific character in a string at specified index
     * @param {?} str String
     * @param {?} index Number
     * @param {?} replacement any
     * @return {?}
     */
    function (str, index, replacement) {
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PrettyNumberDirective.prototype.isValidMovementKeys = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        return $event.key !== '.' && $event.key !== 'ArrowLeft' && $event.key !== 'ArrowRight' && $event.key !== 'Backspace';
    };
    /** @nocollapse */
    PrettyNumberDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: DecimalPipe }
    ]; };
    PrettyNumberDirective.propDecorators = {
        blockPaste: [{ type: HostListener, args: ['paste', ['$event'],] }],
        drop: [{ type: HostListener, args: ['drop', ['$event'],] }],
        onKeyup: [{ type: HostListener, args: ['keyup', ['$event'],] }],
        onKeydown: [{ type: HostListener, args: ['keydown', ['$event'],] }]
    };
PrettyNumberDirective.ɵfac = function PrettyNumberDirective_Factory(t) { return new (t || PrettyNumberDirective)(ɵngcc0.ɵɵdirectiveInject(ɵngcc0.ElementRef), ɵngcc0.ɵɵdirectiveInject(ɵngcc1.DecimalPipe)); };
PrettyNumberDirective.ɵdir = ɵngcc0.ɵɵdefineDirective({ type: PrettyNumberDirective, selectors: [["", "ngxPrettyNumber", ""]], hostBindings: function PrettyNumberDirective_HostBindings(rf, ctx) { if (rf & 1) {
        ɵngcc0.ɵɵlistener("paste", function PrettyNumberDirective_paste_HostBindingHandler($event) { return ctx.blockPaste($event); })("drop", function PrettyNumberDirective_drop_HostBindingHandler($event) { return ctx.drop($event); })("keyup", function PrettyNumberDirective_keyup_HostBindingHandler($event) { return ctx.onKeyup($event); })("keydown", function PrettyNumberDirective_keydown_HostBindingHandler($event) { return ctx.onKeydown($event); });
    } } });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberDirective, [{
        type: Directive,
        args: [{
                selector: '[ngxPrettyNumber]'
            }]
    }], function () { return [{ type: ɵngcc0.ElementRef }, { type: ɵngcc1.DecimalPipe }]; }, { blockPaste: [{
            type: HostListener,
            args: ['paste', ['$event']]
        }], drop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }], onKeyup: [{
            type: HostListener,
            args: ['keyup', ['$event']]
        }], onKeydown: [{
            type: HostListener,
            args: ['keydown', ['$event']]
        }] }); })();
    return PrettyNumberDirective;
}());
export { PrettyNumberDirective };
if (false) {
    /** @type {?} */
    PrettyNumberDirective.prototype.acceptedKeys;
    /** @type {?} */
    PrettyNumberDirective.prototype.navigationKeys;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PrettyNumberDirective.prototype.num;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VzIjpbIm5nOi9uZ3gtcHJldHR5LW51bWJlci9saWIvcHJldHR5LW51bWJlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDbEUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGlCQUFpQixDQUFDOzs7QUFFNUM7SUFRRSwrQkFBb0IsRUFBYyxFQUFVLEdBQWdCO1FBQXhDLE9BQUUsR0FBRixFQUFFLENBQVk7UUFBVSxRQUFHLEdBQUgsR0FBRyxDQUFhO1FBSDVELGlCQUFZLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDL0csbUJBQWMsR0FBRyxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFHMUQsQ0FBQzs7Ozs7SUFFa0MsMENBQVU7Ozs7SUFBN0MsVUFBOEMsQ0FBZ0I7UUFDNUQsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRWlDLG9DQUFJOzs7O0lBQXRDLFVBQXVDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRWtDLHVDQUFPOzs7O0lBQTFDLFVBQTJDLE1BQU07O1lBQzNDLFVBQVUsR0FBVyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLOztZQUM5QyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYztRQUMzRCxVQUFVLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDMUMsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFOztnQkFDdEIsVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ3hDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsOENBQThDO1lBQ2xHLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLCtEQUErRDtnQkFDekYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEQ7WUFDRCw4QkFBOEI7WUFDOUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7O29CQUMzRixXQUFXLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxjQUFjO2dCQUN6RCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsV0FBVyxFQUFFLGNBQWMsR0FBRyxXQUFXLENBQUMsQ0FBQzthQUNyRztpQkFBTTtnQkFDTCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDLENBQUM7YUFDekU7U0FDRjthQUFNO1lBQ0wsSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO2dCQUNyQiw2REFBNkQ7Z0JBQzdELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDNUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsOEJBQThCO2FBQ3hHO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVvQyx5Q0FBUzs7OztJQUE5QyxVQUErQyxLQUFvQjs7WUFDM0QsVUFBVSxHQUFXLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUs7O1lBQ2hELGNBQWMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxjQUFjO1FBQzNELElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3pDLCtEQUErRDtZQUMvRCxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksVUFBVSxLQUFLLEVBQUUsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDdEQ7aUJBQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN4RCxrQkFBa0I7Z0JBQ2xCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN4QjtZQUNELElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTs7b0JBQ3RCLFFBQVEsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQzs7b0JBQ2xDLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsbUVBQW1FO2dCQUNuRSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUMvRyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7aUJBQ3hCO2dCQUVELElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxjQUFjLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDaEUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25ELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxjQUFjLENBQUMsQ0FBQztpQkFDekU7YUFDRjtZQUNELG9FQUFvRTtZQUNwRSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNqRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNqRjtZQUNELHVDQUF1QztZQUN2QyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxXQUFXLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDN0csSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxHQUFHLENBQUMsRUFBRSxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDakY7WUFDRCx5SEFBeUg7WUFDekgsa0JBQWtCO1lBQ2xCLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7bUJBQzVDLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNuRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsY0FBYyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDcEYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUUsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNoRixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDeEI7WUFDRCx1SEFBdUg7WUFDdkgsa0JBQWtCO1lBQ2xCLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7bUJBQzVDLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNuRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsY0FBYyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDcEYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUUsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNoRixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDeEI7U0FHRjthQUFNO1lBQ0wsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7OztJQUNILHlDQUFTOzs7Ozs7O0lBQVQsVUFBVSxHQUFXLEVBQUUsS0FBSyxFQUFFLFdBQVc7UUFDdkMsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsR0FBRyxXQUFXLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3JGLENBQUM7Ozs7O0lBRUQsbURBQW1COzs7O0lBQW5CLFVBQW9CLE1BQU07UUFDeEIsT0FBTyxNQUFNLENBQUMsR0FBRyxLQUFLLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxLQUFLLFdBQVcsSUFBSSxNQUFNLENBQUMsR0FBRyxLQUFLLFlBQVksSUFBSSxNQUFNLENBQUMsR0FBRyxLQUFLLFdBQVcsQ0FBQztJQUN2SCxDQUFDLENBQ0gsQUFsSFM7O2tDQUhSLFNBQVMsU0FBQztRQUNULFFBQVEsRUFBRSxGQUpPLFVBQVU7T0FJRSxrQkFDOUIsVEFKTyxXQUFXOzs7NkJBYWhCLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7dUJBSWhDLFlBQVksU0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUM7MEJBSS9CLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7NEJBNEJoQyxZQUFZLFNBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztvQkFBWTtJQXNFakQsNEJBQUM7Q0FBQSxBQXJIRCxJQXFIQztTQWxIWSxxQkFBcUI7OztJQUVoQyw2Q0FBK0c7O0lBQy9HLCtDQUEwRDs7Ozs7SUFFOUMsbUNBQXNCOzs7OztJQUFFLG9DQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RGlyZWN0aXZlLCBFbGVtZW50UmVmLCBIb3N0TGlzdGVuZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEZWNpbWFsUGlwZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW25neFByZXR0eU51bWJlcl0nXG59KVxuZXhwb3J0IGNsYXNzIFByZXR0eU51bWJlckRpcmVjdGl2ZSB7XG5cbiAgYWNjZXB0ZWRLZXlzID0gWycxJywgJzInLCAnMycsICc0JywgJzUnLCAnNicsICc3JywgJzgnLCAnOScsICcwJywgJy4nLCAnQmFja3NwYWNlJywgJ0Fycm93TGVmdCcsICdBcnJvd1JpZ2h0J107XG4gIG5hdmlnYXRpb25LZXlzID0gWydCYWNrc3BhY2UnLCAnQXJyb3dMZWZ0JywgJ0Fycm93UmlnaHQnXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsOiBFbGVtZW50UmVmLCBwcml2YXRlIG51bTogRGVjaW1hbFBpcGUpIHtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ3Bhc3RlJywgWyckZXZlbnQnXSkgYmxvY2tQYXN0ZShlOiBLZXlib2FyZEV2ZW50KSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcignZHJvcCcsIFsnJGV2ZW50J10pIGRyb3AoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2tleXVwJywgWyckZXZlbnQnXSkgb25LZXl1cCgkZXZlbnQpIHtcbiAgICBsZXQgaW5wdXRWYWx1ZTogc3RyaW5nID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlO1xuICAgIGNvbnN0IGN1cnNvclBvc2l0aW9uID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNlbGVjdGlvblN0YXJ0O1xuICAgIGlucHV0VmFsdWUgPSBpbnB1dFZhbHVlLnJlcGxhY2UoLywvZywgJycpO1xuICAgIGlmIChpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJykpIHtcbiAgICAgIGNvbnN0IHNwbGl0VmFsdWUgPSBpbnB1dFZhbHVlLnNwbGl0KCcuJyk7XG4gICAgICBzcGxpdFZhbHVlWzBdID0gdGhpcy5udW0udHJhbnNmb3JtKCtzcGxpdFZhbHVlWzBdKTsgLy8gRm9ybWF0IHdpdGggY29tbWFzIHRoZSB2YWx1ZSBiZWZvcmUgZGVjaW1hbFxuICAgICAgaWYgKHNwbGl0VmFsdWVbMV0gPT09ICcnKSB7IC8vIFZhbHVlIGFmdGVyIGRvdCBpcyBlbXB0eSwgY29uc2lkZXIgb25seSB0aGUgdmFsdWUgYmVmb3JlIGRvdFxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBzcGxpdFZhbHVlWzBdLmNvbmNhdCgnLjAwJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBzcGxpdFZhbHVlLmpvaW4oJy4nKTtcbiAgICAgIH1cbiAgICAgIC8vIE1haW50YWluaW5nIGN1cnNvciBwb3NpdGlvblxuICAgICAgaWYgKHRoaXMuaXNWYWxpZE1vdmVtZW50S2V5cygkZXZlbnQpICYmIGN1cnNvclBvc2l0aW9uIDwgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlLmluZGV4T2YoJy4nKSkge1xuICAgICAgICBjb25zdCBjdXJzb3JEZXB0aCA9IHNwbGl0VmFsdWVbMF0ubGVuZ3RoIC0gY3Vyc29yUG9zaXRpb247XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiArIGN1cnNvckRlcHRoLCBjdXJzb3JQb3NpdGlvbiArIGN1cnNvckRlcHRoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiwgY3Vyc29yUG9zaXRpb24pO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoaW5wdXRWYWx1ZSAhPT0gJycpIHtcbiAgICAgICAgLy8gQXBwZW5kICcuMDAnIGlmIHVzZXIgZW50ZXIgc29tZXRoaW5nIGxpa2UgJDEgdG8gc2hvdyAkMS4wMFxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSB0aGlzLm51bS50cmFuc2Zvcm0oK2lucHV0VmFsdWUpLmNvbmNhdCgnLjAwJyk7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiwgY3Vyc29yUG9zaXRpb24pOyAvLyBNYWludGFpbmluZyBjdXJzb3IgcG9zaXRpb25cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCdrZXlkb3duJywgWyckZXZlbnQnXSkgb25LZXlkb3duKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgY29uc3QgaW5wdXRWYWx1ZTogc3RyaW5nID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlO1xuICAgIGNvbnN0IGN1cnNvclBvc2l0aW9uID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNlbGVjdGlvblN0YXJ0O1xuICAgIGlmICh0aGlzLmFjY2VwdGVkS2V5cy5pbmNsdWRlcyhldmVudC5rZXkpKSB7XG4gICAgICAvLyBJZiB1c2VyIHByZXNzZWQgZG90IGFuZCB0aGVyZSBpcyBubyBkb3QgYWxyZWFkeSBpbiB0aGUgZmllbGRcbiAgICAgIGlmIChldmVudC5rZXkgPT09ICcuJyAmJiBpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSA9PT0gLTEgJiYgaW5wdXRWYWx1ZSA9PT0gJycpIHtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlID0gaW5wdXRWYWx1ZS5jb25jYXQoJzAnKTtcbiAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5ID09PSAnLicgJiYgaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpKSB7XG4gICAgICAgIC8vIFByZXZlbnQgdHdvIGRvdFxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgICAgaWYgKGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKSkge1xuICAgICAgICBjb25zdCBkb3RJbmRleCA9IGlucHV0VmFsdWUuaW5kZXhPZignLicpO1xuICAgICAgICBjb25zdCBzcGxpdCA9IGlucHV0VmFsdWUuc3BsaXQoJy4nKTtcbiAgICAgICAgLy8gTmF2aWdhdGlvbktleXMgPT4gQWxsb3dpbmcgbGVmdCBhcnJvdywgcmlnaHQgYXJyb3cgYW5kIGJhY2tzcGFjZVxuICAgICAgICBpZiAoc3BsaXRbMV0ubGVuZ3RoID49IDIgJiYgKGN1cnNvclBvc2l0aW9uIC0gMSkgPT09IChkb3RJbmRleCArIDIpICYmICF0aGlzLm5hdmlnYXRpb25LZXlzLmluY2x1ZGVzKGV2ZW50LmtleSkpIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNwbGl0WzBdID09PSAnMCcgJiYgY3Vyc29yUG9zaXRpb24gPCBpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSkge1xuICAgICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcuJy5jb25jYXQoc3BsaXRbMV0pO1xuICAgICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiwgY3Vyc29yUG9zaXRpb24pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICAvLyBTa2lwIGRvdCAoYXV0b21hdGljYWxseSBtb3ZlIGN1cnNvciB0byBuZXh0IG51bWJlciBhZnRlciBkZWNpbWFsKVxuICAgICAgaWYgKGlucHV0VmFsdWUuaW5jbHVkZXMoJy4nKSAmJiBldmVudC5rZXkgPT09ICcuJyAmJiAoY3Vyc29yUG9zaXRpb24pID09PSBpbnB1dFZhbHVlLmluZGV4T2YoJy4nKSkge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoY3Vyc29yUG9zaXRpb24gKyAxLCBjdXJzb3JQb3NpdGlvbiArIDEpO1xuICAgICAgfVxuICAgICAgLy8gU2tpcCBkb3Qgb24gYmFja3NwYWNlIChrZWVwIGRlY2ltYWwpXG4gICAgICBpZiAoaW5wdXRWYWx1ZS5pbmNsdWRlcygnLicpICYmIGV2ZW50LmtleSA9PT0gJ0JhY2tzcGFjZScgJiYgKGN1cnNvclBvc2l0aW9uKSA9PT0gaW5wdXRWYWx1ZS5pbmRleE9mKCcuJykgKyAxKSB7XG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3JQb3NpdGlvbiAtIDEsIGN1cnNvclBvc2l0aW9uIC0gMSk7XG4gICAgICB9XG4gICAgICAvLyBJZiB1c2VyIGVudGVyZWQgJDEyLjxjdXJzb3I+NTYsIGFuZCBjdXJzb3IgaXMgYXQgbWlkZGxlIG9mIGRvdCBhbmQgNSwgdGhlbiBpZiB1c2VyIHByZXNzZXMgJzknIHRoZW4gaXQgd2lsbCB1cGRhdGUgdGhlXG4gICAgICAvLyB2YWx1ZSB0byAkMTIuOTZcbiAgICAgIGlmIChldmVudC5rZXkgIT09ICcuJyAmJiBpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJylcbiAgICAgICAgJiYgKGN1cnNvclBvc2l0aW9uKSA9PT0gKGlucHV0VmFsdWUuaW5kZXhPZignLicpICsgMSkgJiYgIXRoaXMubmF2aWdhdGlvbktleXMuaW5jbHVkZXMoZXZlbnQua2V5KSkge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSB0aGlzLnJlcGxhY2VBdChpbnB1dFZhbHVlLCBjdXJzb3JQb3NpdGlvbiwgZXZlbnQua2V5KTtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdpbnB1dCcpKTtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uICsgMSwgY3Vyc29yUG9zaXRpb24gKyAxKTtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICAgIC8vIElmIHVzZXIgZW50ZXJlZCAkMTIuNTxjdXJzb3I+NiwgYW5kIGN1cnNvciBpcyBhdCBtaWRkbGUgb2YgNSBhbmQgNiwgdGhlbiBpZiB1c2VyIHByZXNzZXMgJzknIHRoZW4gaXQgd2lsbCB1cGRhdGUgdGhlXG4gICAgICAvLyB2YWx1ZSB0byAkMTIuNTlcbiAgICAgIGlmIChldmVudC5rZXkgIT09ICcuJyAmJiBpbnB1dFZhbHVlLmluY2x1ZGVzKCcuJylcbiAgICAgICAgJiYgKGN1cnNvclBvc2l0aW9uKSA9PT0gKGlucHV0VmFsdWUuaW5kZXhPZignLicpICsgMikgJiYgIXRoaXMubmF2aWdhdGlvbktleXMuaW5jbHVkZXMoZXZlbnQua2V5KSkge1xuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSB0aGlzLnJlcGxhY2VBdChpbnB1dFZhbHVlLCBjdXJzb3JQb3NpdGlvbiwgZXZlbnQua2V5KTtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdpbnB1dCcpKTtcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnNldFNlbGVjdGlvblJhbmdlKGN1cnNvclBvc2l0aW9uICsgMiwgY3Vyc29yUG9zaXRpb24gKyAyKTtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cblxuXG4gICAgfSBlbHNlIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFJlcGxhY2Ugc3BlY2lmaWMgY2hhcmFjdGVyIGluIGEgc3RyaW5nIGF0IHNwZWNpZmllZCBpbmRleFxuICAgKiBAcGFyYW0gc3RyIFN0cmluZ1xuICAgKiBAcGFyYW0gaW5kZXggTnVtYmVyXG4gICAqIEBwYXJhbSByZXBsYWNlbWVudCBhbnlcbiAgICovXG4gIHJlcGxhY2VBdChzdHI6IHN0cmluZywgaW5kZXgsIHJlcGxhY2VtZW50KSB7XG4gICAgcmV0dXJuIHN0ci5zdWJzdHIoMCwgaW5kZXgpICsgcmVwbGFjZW1lbnQgKyBzdHIuc3Vic3RyKGluZGV4ICsgcmVwbGFjZW1lbnQubGVuZ3RoKTtcbiAgfVxuXG4gIGlzVmFsaWRNb3ZlbWVudEtleXMoJGV2ZW50KSB7XG4gICAgcmV0dXJuICRldmVudC5rZXkgIT09ICcuJyAmJiAkZXZlbnQua2V5ICE9PSAnQXJyb3dMZWZ0JyAmJiAkZXZlbnQua2V5ICE9PSAnQXJyb3dSaWdodCcgJiYgJGV2ZW50LmtleSAhPT0gJ0JhY2tzcGFjZSc7XG4gIH1cbn1cbiJdfQ==
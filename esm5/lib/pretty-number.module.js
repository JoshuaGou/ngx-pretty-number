/**
 * @fileoverview added by tsickle
 * Generated from: lib/pretty-number.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { PrettyNumberDirective } from './pretty-number.directive';
import { DecimalPipe } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
var PrettyNumberModule = /** @class */ (function () {
    function PrettyNumberModule() {
    }
PrettyNumberModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: PrettyNumberModule });
PrettyNumberModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function PrettyNumberModule_Factory(t) { return new (t || PrettyNumberModule)(); }, providers: [DecimalPipe], imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(PrettyNumberModule, { declarations: function () { return [PrettyNumberDirective]; }, exports: function () { return [PrettyNumberDirective]; } }); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(PrettyNumberModule, [{
        type: NgModule,
        args: [{
                declarations: [PrettyNumberDirective],
                imports: [],
                exports: [PrettyNumberDirective],
                providers: [DecimalPipe]
            }]
    }], function () { return []; }, null); })();
    return PrettyNumberModule;
}());
export { PrettyNumberModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldHR5LW51bWJlci5tb2R1bGUuanMiLCJzb3VyY2VzIjpbIm5nOi9uZ3gtcHJldHR5LW51bWJlci9saWIvcHJldHR5LW51bWJlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xFLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQzs7QUFFNUM7SUFBQTtJQU1rQyxDQUFDO3NEQU5sQyxRQUFRLFNBQUM7WUFDUixZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxzQkFDckMsT0FBTyxFQUFFLEVBQUUsc0JBQ1gsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUMsc0JBQ2hDLFNBQVMsRUFBRSxDQUFDLFdBQVcsQ0FBQztXQUN6Qjs7Ozs7Ozs7O2dEQUNRO0lBQXlCLHlCQUFDO0NBQUEsQUFObkMsSUFNbUM7U0FBdEIsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFByZXR0eU51bWJlckRpcmVjdGl2ZSB9IGZyb20gJy4vcHJldHR5LW51bWJlci5kaXJlY3RpdmUnO1xuaW1wb3J0IHtEZWNpbWFsUGlwZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbUHJldHR5TnVtYmVyRGlyZWN0aXZlXSxcbiAgaW1wb3J0czogW10sXG4gIGV4cG9ydHM6IFtQcmV0dHlOdW1iZXJEaXJlY3RpdmVdLFxuICBwcm92aWRlcnM6IFtEZWNpbWFsUGlwZV1cbn0pXG5leHBvcnQgY2xhc3MgUHJldHR5TnVtYmVyTW9kdWxlIHsgfVxuIl19